package com.lanou3g.ssm.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class User {
    private Integer id;

    private String nickName;

    private String username;

    private String password;

    private String email;

    private Integer status;

    private Date createtime;

    private Date lastLoginTime;

}