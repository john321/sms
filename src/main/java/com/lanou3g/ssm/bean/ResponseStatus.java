package com.lanou3g.ssm.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
public class ResponseStatus {
    private Integer code;
    private String msg;
    private Map<String, String> data;

    public static ResponseStatus ok() {
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_OK);
        return responseStatus;
    }

    public static ResponseStatus ok(String msg) {
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_OK);
        responseStatus.setMsg(msg);
        return responseStatus;
    }

    public static ResponseStatus ok(String msg, Map<String, String> data) {
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_OK);
        responseStatus.setMsg(msg);
        responseStatus.setData(data);
        return responseStatus;
    }

    public static ResponseStatus error() {
        ResponseStatus responseStatus = new ResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_ERROR);
        return responseStatus;
    }
}
