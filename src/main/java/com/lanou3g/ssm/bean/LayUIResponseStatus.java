package com.lanou3g.ssm.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 用于返回LayUI需要的Ajax数据格式
 */
@Setter
@Getter
public class LayUIResponseStatus {
    private Integer code;
    private String msg;
    private Integer count;
    private Object data;

    public static LayUIResponseStatus ok() {
        LayUIResponseStatus responseStatus = new LayUIResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_LAYUI_OK);
        return responseStatus;
    }

    public static LayUIResponseStatus ok(String msg) {
        LayUIResponseStatus responseStatus = new LayUIResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_OK);
        responseStatus.setMsg(msg);
        return responseStatus;
    }

    public static LayUIResponseStatus ok(String msg, Object data) {
        LayUIResponseStatus responseStatus = new LayUIResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_OK);
        responseStatus.setMsg(msg);
        responseStatus.setData(data);
        return responseStatus;
    }

    public static LayUIResponseStatus error() {
        LayUIResponseStatus responseStatus = new LayUIResponseStatus();
        responseStatus.setCode(SystemConstants.RESPONSE_STATUS_ERROR);
        return responseStatus;
    }
}
