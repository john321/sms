package com.lanou3g.ssm.bean;

public class SystemConstants {

    /**
     * 用户状态： 正常：1;
     */
    public final static int USER_STATUS_NORMAL = 1;
    /**
     * '用户状态： 注销：2；
     */
    public final static int USER_STATUS_DELETE = 2;
    /**
     * '用户状态： 锁定：3'
     */
    public final static int USER_STATUS_LOCKED = 3;

    /**
     * 消息状态：已读
     */
    public final static int MESSAGE_STATUS_READ = 1;

    /**
     * 消息状态：未读
     */
    public final static int MESSAGE_STATUS_UNREAD = 2;

    /**
     * 消息状态：删除
     */
    public final static int MESSAGE_STATUS_DELETE = 3;

    /**
     * 通用响应状态：正常
     */
    public final static int RESPONSE_STATUS_OK = 200;

    /**
     * 返回LayUI Ajax请求状态：正常
     */
    public final static int RESPONSE_STATUS_LAYUI_OK = 0;
    /**
     * 通用响应状态：异常
     */
    public final static int RESPONSE_STATUS_ERROR = 500;

}
