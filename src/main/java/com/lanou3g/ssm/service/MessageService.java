package com.lanou3g.ssm.service;

import com.lanou3g.ssm.bean.Message;
import com.lanou3g.ssm.bean.SystemConstants;
import com.lanou3g.ssm.bean.User;
import com.lanou3g.ssm.mapper.MessageMapper;
import com.lanou3g.ssm.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.context.support.ServletContextResource;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class MessageService {

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private UserMapper userMapper;

    /**
     * 查询收件箱
     * @param session
     * @return
     */
    public List<Message> queryReceiveMessage(HttpSession session) {
        User user = (User) session.getAttribute("userInfo");
        Message condition = new Message();
        condition.setToId(user.getId());
        return messageMapper.selectMessageList(condition);
    }

    /**
     * 查询发件箱
     * @param session
     * @return
     */
    public List<Message> querySendMessage(HttpSession session) {
        User user = (User) session.getAttribute("userInfo");
        Message condition = new Message();
        condition.setFromId(user.getId());
        return messageMapper.selectMessageList(condition);
    }

    /**
     * 发送消息
     * @param message
     * @param file
     * @param req
     */
    public void sendMessage(Message message, MultipartFile file, HttpServletRequest req) {
        message.setStatus(SystemConstants.MESSAGE_STATUS_UNREAD);
        message.setCreatetime(new Date());
        String fileName = file.getOriginalFilename();
        String filePath = "/upload/"+fileName;
        message.setAttachment(filePath);

        messageMapper.insert(message);


        // 获取上传文件要存放到服务器哪个地方，然后调用multipartFile.transferTo(file);
        ServletContext ctx = req.getServletContext();
        // 原生写法
        /*String realPath = ctx.getRealPath("/upload/"+ fileName);
        file.transferTo(new File(realPath));*/

        // 使用SpringMVC ServletContextResource的写法
        ServletContextResource resource = new ServletContextResource(ctx, "/upload/" + fileName);
        try {
            file.transferTo(resource.getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 阅读消息
     * @param id
     * @param fromPage
     * @param model
     */
    public void toReadPage(Integer id, String fromPage, Model model) {
        // 查询消息
        Message message = messageMapper.selectByPrimaryKey(id);
        // 只有从收件箱点来查看消息，才将消息状态更新为"已读"
        if("receive".equals(fromPage)) {
            message.setStatus(SystemConstants.MESSAGE_STATUS_READ);
            messageMapper.updateByPrimaryKey(message);
        }

        // 查询发送人
        User condition = new User();
        condition.setId(message.getFromId());
        List<User> userList = userMapper.selectUser(condition);
        User fromUser = null;
        if(userList != null && userList.size() > 0) {
            fromUser = userList.get(0);
        }

        // 查询接收人
        condition.setId(message.getToId());
        userList = userMapper.selectUser(condition);
        User toUser = null;
        if(userList != null && userList.size() > 0) {
            toUser = userList.get(0);
        }

        // 将需要送往页面的数据放入model中(其实就是放入了requestScope中)
        model.addAttribute("fromUser", fromUser);
        model.addAttribute("toUser", toUser);
        model.addAttribute("message", message);
    }

	public void deleteMessageById(Integer id) throws Exception {
        Message message = new Message();
        message.setId(id);
        message.setStatus(SystemConstants.MESSAGE_STATUS_DELETE);
        messageMapper.updateByPrimaryKey(message);
    }
}
