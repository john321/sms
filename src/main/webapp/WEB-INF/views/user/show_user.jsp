<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a><cite>账户管理</cite></a>
      <a><cite>我的资料</cite></a>
    </span>
    <!-- 内容主体区域 -->
    <form id="sendMessageForm" class="layui-form" method="post" enctype="multipart/form-data" action="/user/update" style="margin-top: 20px;">
        <input type="hidden" name="id" value="${userInfo.id}" />
        <div class="layui-form-item">
            <label class="layui-form-label">昵称：</label>
            <div class="layui-input-inline">
                <input type="text" name="nickName" value="${userInfo.nickName}" lay-verify="title" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名：</label>
            <div class="layui-input-inline">
                <input type="text" name="username" value="${userInfo.username}" readonly lay-verify="title" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">注册邮箱：</label>
            <div class="layui-input-inline">
                <input type="text" name="email" value="${userInfo.email}" lay-verify="title" autocomplete="off" placeholder="请输入昵称" class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button id="submitBtn" class="layui-btn" lay-filter="demo1">修改资料</button>
            </div>
        </div>
    </form>
</div>
<jsp:include page="../common/footer.jsp"/>