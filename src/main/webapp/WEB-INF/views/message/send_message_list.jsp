<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a href="/message/to_receive_list">消息管理</a>
      <a><cite>发件箱</cite></a>
    </span>
    <!-- 内容主体区域 -->
    <table class="layui-table" lay-even="" lay-skin="nob">
        <colgroup>
            <col width="150">
            <col>
            <col width="200">
            <col width="300">
        </colgroup>
        <thead>
        <tr>
            <th>状态</th>
            <th>标题</th>
            <th>操作</th>
            <th>时间</th>
        </tr>
        </thead>
        <tbody>
            <c:forEach items="${messageList}" var="msg">
                <tr>
                    <td>
                        <c:if test="${msg.status == 1}">
                            <%--<img src="/static/images/sms_readed.png" />--%>
                            <i class="layui-icon layui-icon-release" style="font-size: 30px; color: lightgray;"></i>
                        </c:if>
                        <c:if test="${msg.status == 2}">
                            <%--<img src="/static/images/sms_unReaded.png" />--%>
                            <i class="layui-icon layui-icon-release" style="font-size: 30px; color: #1E9FFF;"></i>
                        </c:if>
                    </td>
                    <td>
                        <a href="/message/to_read/${msg.id}/send">${msg.subject}</a>
                    </td>
                    <td>
                        <a href="#">删除</a>
                        &nbsp;&nbsp;
                        <a href="#">再发一封</a>
                    </td>
                    <td>
                        <fmt:formatDate value="${msg.createtime}" pattern="yyyy-MM-dd HH:mm:ss" />
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<jsp:include page="../common/footer.jsp"/>
