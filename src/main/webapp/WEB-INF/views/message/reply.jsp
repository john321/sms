<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../common/header.jsp"/>
<jsp:include page="../common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a href="/to_main">首页</a>
      <a href="/message/to_receive_list">消息管理</a>
      <a><cite>发消息</cite></a>
    </span>
    <!-- 内容主体区域 -->

    <form id="sendMessageForm" class="layui-form" method="post" enctype="multipart/form-data" action="/message/send" style="margin-top: 20px;">
        <input type="hidden" name="fromId" value="${userInfo.id}" />
        <input type="hidden" name="nickName" value="${nickName}" />
        <input type="hidden" name="toId" value="${toId}" />
        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">接收人：</label>
                <div class="layui-input-inline">
                    <label class="layui-form-label">${nickName}</label>
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">消息标题：</label>
                <div class="layui-input-block">
                    <input type="text" name="subject" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">消息内容：</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea layui-hide" name="content" lay-verify="content" id="edit_message_content"></textarea>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">附件：</label>
                <%--<div class="layui-input-block">
                    <input type="file" name="myFile" />
                </div>--%>
                <div class="layui-upload-drag" id="attachment">
                    <i class="layui-icon"></i>
                    <p>点击上传，或将文件拖拽到此处</p>
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button id="submitBtn" class="layui-btn" lay-filter="demo1">发&nbsp;&nbsp;送</button>
            </div>
        </div>

</div>

<jsp:include page="../common/footer.jsp"/>