<%--
  Created by IntelliJ IDEA.
  User: John
  Date: 2019/7/4
  Time: 18:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="common/header.jsp"/>
<jsp:include page="common/menu.jsp"/>
<div class="layui-body" style="top: 70px; padding: 0 10px;">

    <span class="layui-breadcrumb">
      <a><cite>首页</cite></a>
    </span>
    <!-- 内容主体区域 -->

    <pre>
    轻轻的我走了，
    正如我轻轻的来；
    我轻轻的招手，
    作别西天的云彩。

    那河畔的金柳，
    是夕阳中的新娘；
    波光里的艳影，
    在我的心头荡漾。

    软泥上的青荇，
    油油的在水底招摇；
    在康河的柔波里，
    我甘心做一条水草！
    </pre>


</div>

<jsp:include page="common/footer.jsp"/>
